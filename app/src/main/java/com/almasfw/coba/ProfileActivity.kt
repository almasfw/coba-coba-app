package com.almasfw.coba

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        namaLengkap.text = intent.extras.getString("nama", "almas fauzia")
        username.text = intent.extras.getString("username", "almasfw")

        buttonEditProfile.setOnClickListener {
            val intent = Intent(this@ProfileActivity, RegisterActivity::class.java)
            startActivity(intent)
        }

        buttonLogout.setOnClickListener {
            val intent = Intent(this@ProfileActivity, MainActivity::class.java)
            startActivity(intent)
        }

    }
}