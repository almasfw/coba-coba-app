package com.almasfw.coba

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    //lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonLogin.setOnClickListener {
            val intent = Intent(this@MainActivity, ProfileActivity::class.java)
            intent.putExtra("nama", namaLengkap.text.toString())
            intent.putExtra("username", username.text.toString())
            startActivity(intent)
        }

        //    button = findViewById(R.id.buttonLogin)
        //    button.setOnClickListener {
        //        Toast.makeText(this@MainActivity, "Test", Toast.LENGTH_LONG)
        //
        //        val intent = Intent(this@MainActivity, ProfileActivity::class.java)
        //        startActivity(intent)
        //    }

        buttonRegister.setOnClickListener {
            val intent = Intent(this@MainActivity, RegisterActivity::class.java)
            startActivity(intent)
        }
    }
}
