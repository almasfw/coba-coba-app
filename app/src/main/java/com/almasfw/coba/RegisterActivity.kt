package com.almasfw.coba

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        buttonRegister.setOnClickListener() {
            val intent = Intent(this@RegisterActivity, ProfileActivity::class.java)
            intent.putExtra("nama", namaLengkap.text.toString())
            intent.putExtra("username", username.text.toString())
            startActivity(intent)
        }

    }
}